import { Client } from '@metahkg/rlp-proxy-rewrite-api';
import React, { useEffect, useState } from 'react';

import './linkPreview.scss';
import Skeleton from './Skeleton';

export const placeholderImg = 'https://i.imgur.com/UeDNBNQ.jpeg';

function isValidResponse(res: APIResponse | null): boolean {
  if (!res) return false;
  if (res.title === null || res.title === undefined) return false;
  if (res.hostname === null || res.hostname === undefined) return false;
  return true;
}

export interface LinkPreviewProps {
  url: string;
  className?: string;
  width?: string | number;
  height?: string | number;
  titleLength?: number;
  descriptionLength?: number;
  borderRadius?: string | number;
  imageHeight?: string | number;
  textAlign?: 'left' | 'right' | 'center';
  margin?: string | number;
  fallback?: JSX.Element[] | JSX.Element | null;
  backgroundColor?: string;
  primaryTextColor?: string;
  secondaryTextColor?: string;
  borderColor?: string;
  showLoader?: boolean;
  customLoader?: JSX.Element[] | JSX.Element | null;
  openInNewTab?: boolean;
  fetcher?: (url: string) => Promise<APIResponse | null>;
  proxyLink?: string;
  /** https://github.com/willnorris/imageproxy. e.g. https://i.metahkg.org */
  imageProxy?: string;
  fallbackImageSrc?: string;
  explicitImageSrc?: string;
  /* Whether the placeholder image is displayed in case no image could be scraped */
  showPlaceholderIfNoImage?: boolean;
  onSuccess?: (metadata: APIResponse | null) => void;
}

export interface APIResponse {
  title: string | null;
  description: string | null;
  image: string | null;
  image_signature?: string;
  siteName: string | null;
  hostname: string | null;
}

export const LinkPreview: React.FC<LinkPreviewProps> = ({
  url,
  className = '',
  width,
  height,
  titleLength,
  descriptionLength,
  borderRadius,
  imageHeight,
  textAlign,
  margin,
  fallback = null,
  backgroundColor = 'white',
  primaryTextColor = 'black',
  secondaryTextColor = 'rgb(100, 100, 100)',
  borderColor = '#ccc',
  showLoader = true,
  customLoader = null,
  openInNewTab = true,
  fetcher,
  proxyLink = 'https://rlp.metahkg.org',
  imageProxy,
  fallbackImageSrc = placeholderImg,
  explicitImageSrc = null,
  showPlaceholderIfNoImage = true,
  onSuccess = (metadata) => {},
}) => {
  const [metadata, setMetadata] = useState<APIResponse | null>();
  const [loading, setLoading] = useState(false);
  const [escaped, setEscaped] = useState(false);

  useEffect(() => {
    if (loading || metadata !== undefined) return;
    setLoading(true);

    if (fetcher) {
      fetcher(url)
        .then((res) => {
          let metadata;
          if (isValidResponse(res)) {
            metadata = res;
            setMetadata(res);
          } else {
            metadata = null;
            setMetadata(null);
          }
          onSuccess(metadata);
          setLoading(false);
        })
        .catch((err: Error) => {
          console.error(err);
          console.error('No metadata could be found for the given URL.');
          onSuccess(null);
          setMetadata(null);
          setLoading(false);
        });
    } else {
      const client = new Client(proxyLink);
      client
        .getMetadata(encodeURIComponent(url))
        .then((res) => {
          setMetadata(res.metadata as unknown as APIResponse);
          onSuccess(res.metadata);
          setLoading(false);
        })
        .catch((err: Error) => {
          console.error(err);
          console.error('No metadata could be found for the given URL.');
          onSuccess(null);
          setMetadata(null);
          setLoading(false);
        });
    }
  }, [url, fetcher, loading, metadata, onSuccess, proxyLink]);

  if (loading && showLoader) {
    if (customLoader) {
      return <>{customLoader}</>;
    } else {
      return <Skeleton width={width} imageHeight={imageHeight} margin={margin} />;
    }
  }

  if (!metadata) {
    return <>{fallback}</>;
  }

  if (!escaped) {
    try {
      // escape for CSS
      metadata.image = metadata.image && CSS.escape(metadata.image);
      fallbackImageSrc = CSS.escape(fallbackImageSrc);
      explicitImageSrc = explicitImageSrc && CSS.escape(explicitImageSrc);
      setEscaped(true);
    } catch {
      setEscaped(true);
    }
  }

  const { image, image_signature, description, title, siteName, hostname } = metadata;

  return (
    <a
      data-testid='container'
      href={url}
      target={openInNewTab ? '_blank' : '_self'}
      rel='noreferrer'
      className={`Container ${className}`}
      style={{
        width,
        height,
        borderRadius,
        textAlign,
        margin,
        backgroundColor,
        borderColor,
        textDecoration: 'none',
      }}
    >
      {(explicitImageSrc || image || (fallbackImageSrc && showPlaceholderIfNoImage)) && (
        <div
          data-testid='image-container'
          style={{
            borderTopLeftRadius: borderRadius,
            borderTopRightRadius: borderRadius,
            backgroundImage: `url(${
              imageProxy ? `${imageProxy}/${image_signature ? `s${image_signature}/` : ''}` : ''
            }${explicitImageSrc || image || fallbackImageSrc}), url(${
              // use original if image proxy requires signature
              imageProxy && !image_signature ? `${imageProxy}/` : ''
            }${fallbackImageSrc})`,
            height: imageHeight,
          }}
          className='Image'
        ></div>
      )}
      <div className='LowerContainer'>
        {title && (
          <h3 data-testid='title' className='Title' style={{ color: primaryTextColor }}>
            {titleLength
              ? title.length > titleLength
                ? title.slice(0, descriptionLength) + '...'
                : title
              : title}
          </h3>
        )}
        {description && (
          <span
            data-testid='desc'
            className='Description Secondary'
            style={{ color: secondaryTextColor }}
          >
            {descriptionLength
              ? description.length > descriptionLength
                ? description.slice(0, descriptionLength) + '...'
                : description
              : description}
          </span>
        )}
        <div className='Secondary SiteDetails' style={{ color: secondaryTextColor }}>
          {siteName && <span>{siteName} • </span>}
          <span>{hostname}</span>
        </div>
      </div>
    </a>
  );
};
