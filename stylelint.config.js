module.exports = {
  extends: 'stylelint-config-standard',
  ignoreFiles: ['src/**/*.snap', 'src/assets/*.jpg'],
};
